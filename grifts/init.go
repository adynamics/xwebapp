package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/adynamics/xwebapp/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
